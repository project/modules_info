<?php

namespace Drupal\modules_info\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the module info entity class.
 *
 * @ContentEntityType(
 *   id = "modules_info",
 *   label = @Translation("Modules info"),
 *   label_collection = @Translation("Modules"),
 *   label_singular = @Translation("module"),
 *   label_plural = @Translation("modules"),
 *   label_count = @PluralTranslation(
 *     singular = "@count module",
 *     plural = "@count modules",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\modules_info\ModulesInfoListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\modules_info\Form\ModulesInfoForm",
 *       "edit" = "Drupal\modules_info\Form\ModulesInfoForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\modules_info\Routing\ModulesInfoHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "modules_info",
 *   admin_permission = "administer modules_info",
 *   entity_keys = {
 *     "id" = "id",
 *     "name" = "name",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "collection" = "/admin/content/modules-info",
 *     "add-form" = "/modules-info/add",
 *     "canonical" = "/modules-info/{modules_info}",
 *     "edit-form" = "/modules-info/{modules_info}",
 *     "delete-form" = "/modules-info/{modules_info}/delete",
 *   },
 * )
 */
class ModulesInfo extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDescription(t('The module name.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled');

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setRequired(TRUE)
      ->setDefaultValue(0);

    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Machine name'))
      ->setRequired(TRUE)
      ->setDescription(t('The module machine name.'))
      ->addConstraint('UniqueField', [])
      ->addConstraint('ValidateMachineName', [])
      ->addPropertyConstraints('value', ['Regex' => ['pattern' => '/^[a-z0-9_]+$/']])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'machine_name',
        'weight' => -4,
        'settings' => [
          'source_field' => 'name',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['version'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Version'))
      ->setDescription(t('The module version.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['sites'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Sites'))
      ->setDescription(t('Module number of used sites.'))
      ->setDefaultValue(0);

    $fields['issues'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Issues'))
      ->setDescription(t('Open Issues of the module.'))
      ->setDefaultValue(0);

    $fields['bugs'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Bugs'))
      ->setDescription(t('Open bugs of the module.'))
      ->setDefaultValue(0);

    return $fields;
  }

}
