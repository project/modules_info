<?php

namespace Drupal\modules_info\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the module info entity edit forms.
 */
class ModulesInfoForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->get('name')->value];
    $logger_arguments = [
      '%label' => $entity->get('name')->value,
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New module info %label has been created.', $message_arguments));
        $this->logger('modules_info')->notice('Created new module info %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The module info %label has been updated.', $message_arguments));
        $this->logger('modules_info')->notice('Updated module info %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.modules_info.collection');

    return $result;
  }

}
