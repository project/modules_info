# Modules Info

* Introduction
* Features

## Introduction

This module is an informer block with table of Drupal modules info.
It is useful for all Drupalers, communities and companies which wants
to provide some modules listing with links to the Drupal.org site.

## Features

* Modules added as content entities on the Content - Modules (tab) page.
* When you add modules, their data will be updated immediately.
* Later, their data will be updated by editing, and by cron.
* You will have links to **usage, releases, issues, bugs and module** Drupal.org
  pages.
* [Drupal](https://www.drupal.org/project/drupal) module supported.
* You can sort module content entities **with dragging**.
* You can switch off any module from block by change their status
  **by clicking on AJAX Status link** on the entities page.
* You can disable any block column with module settings.
* You can attach simple CSS with styling, or disable that CSS on module settings
  page.
* After [patching the core](https://www.drupal.org/files/issues/2022-02-14/2685749-106.patch)
  or when [#2685749: Add a 'machine_name' widget for string field types with a
  UniqueField constraint](https://www.drupal.org/project/drupal/issues/2685749)
  issue will be resolved, you can fill module machine name field automatically
  by entering module name.

## Maintainers

Andrew Answer | <https://www.drupal.org/u/andrew-answer> |
<http://andrew.answer.name> | <http://it.answe.ru> |
<https://t.me/andrew_answer> | <mail@answe.ru>

Nov 2022
