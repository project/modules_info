<?php

namespace Drupal\modules_info;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a trait for draggable listings of entities.
 *
 * @see https://www.drupal.org/project/drupal/issues/2989889
 * @see https://www.drupal.org/project/entity/issues/2990784
 */
trait DraggableListBuilderTrait {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The key to use for the form element containing the entities.
   *
   * @var string
   */
  protected $entitiesKey = 'entities';

  /**
   * The entities being listed.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $entities = [];

  /**
   * Name of the entity's weight field or FALSE if no field is provided.
   *
   * @var string|bool
   */
  protected $weightKey = FALSE;

  /**
   * Gets the weight of the given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to get the weight for.
   *
   * @return int
   *   The weight of the entity.
   */
  abstract protected function getWeight(EntityInterface $entity);

  /**
   * Sets the weight of an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose weight will be set.
   * @param int $weight
   *   The weight to set on the entity.
   */
  abstract protected function setWeight(EntityInterface $entity, $weight);

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    if (!empty($this->weightKey)) {
      $header['weight'] = t('Weight');
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    if (!empty($this->weightKey)) {
      // Override default values to markup elements.
      $row['#attributes']['class'][] = 'draggable';
      $row['#weight'] = $this->getWeight($entity);
      // Add weight column.
      $row['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $this->getWeight($entity),
        '#attributes' => ['class' => ['weight']],
      ];
    }
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    if (!empty($this->weightKey)) {
      return $this->formBuilder->getForm($this);
    }
    return parent::render();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],
    ];

    $this->entities = $this->load();
    $delta = 10;
    // Change the delta of the weight field if have more than 20 entities.
    if (!empty($this->weightKey)) {
      $count = count($this->entities);
      if ($count > 20) {
        $delta = ceil($count / 2);
      }
    }
    foreach ($this->entities as $entity) {
      $row = $this->buildRow($entity);
      if (isset($row['label'])) {
        $row['label'] = ['#markup' => $row['label']];
      }
      if (isset($row['weight'])) {
        $row['weight']['#delta'] = $delta;
      }
      $form[$this->entitiesKey][$entity->id()] = $row;
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue($this->entitiesKey) as $id => $value) {
      if (isset($this->entities[$id]) && $this->getWeight($this->entities[$id]) != $value['weight']) {
        // Save entity only when its weight was changed.
        $this->setWeight($this->entities[$id], $value['weight']);
        $this->entities[$id]->save();
      }
    }
  }

}
