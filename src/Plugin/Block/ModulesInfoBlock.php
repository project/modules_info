<?php

namespace Drupal\modules_info\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\modules_info\Entity\ModulesInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Modules Info block.
 *
 * @Block(
 *   id = "modules_info_block",
 *   admin_label = @Translation("Modules Info"),
 *   category = @Translation("Modules Info")
 * )
 */
class ModulesInfoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ModulesInfoBlock.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'modules_info';
    $modules = ModulesInfo::loadMultiple();
    $values = [];
    $url = 'https://www.drupal.org/project/';
    $columns = $this->configFactory->get('modules_info.settings')->get('columns');
    $pattern = array_fill_keys([
      'id',
      'name',
      'machine_name',
      'version',
      'sites',
      'issues',
      'bugs',
    ], 1);
    $columns = is_array($columns) && array_filter($columns) ? $columns : $pattern;
    foreach ($modules as $row) {
      if ($row->get('status')->value) {
        $machine_name = $row->get('machine_name')->value;
        $values[$row->get('weight')->value] = [
          'name' => $row->get('name')->value,
          'machine_name' => $row->get('machine_name')->value,
          'version' => $row->get('version')->value,
          'sites' => $row->get('sites')->value,
          'issues' => $row->get('issues')->value,
          'bugs' => $row->get('bugs')->value,
          'name_url' => $url . $machine_name,
          'version_url' => $url . $machine_name . '/releases/' . $row->get('version')->value,
          'sites_url' => $url . 'usage/' . $machine_name,
          'issues_url' => $url . 'issues/' . $machine_name . '?categories=All',
          'bugs_url' => $url . 'issues/' . $machine_name . '?categories=1',
        ];
      }
    }
    ksort($values);
    $i = 1;
    foreach ($values as &$value) {
      $value['id'] = $i++;
    }

    $build['#content'] = $values;
    $build['#columns'] = $columns;
    $build['#internal_css'] = $this->configFactory->get('modules_info.settings')->get('internal_css');
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
