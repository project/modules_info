<?php

namespace Drupal\modules_info;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a list controller for the modules info entity type.
 */
class ModulesInfoListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    if (isset($build['table'])) {
      $build['table'][$this->entitiesKey]['#empty'] = $this->t('No modules available.');
    }

    $build['summary']['#markup'] = $this->t('Total modules: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Id');
    $header['name'] = $this->t('Name');
    $header['machine_name'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    $header['version'] = $this->t('Version');
    $header['sites'] = $this->t('Sites');
    $header['issues'] = $this->t('Issues');
    $header['bugs'] = $this->t('Bugs');
    if (!empty($this->weightKey)) {
      $header['weight'] = $this->t('Weight');
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $machine_name = $entity->get('machine_name')->value;
    $row['id']['#markup'] = $entity->id();
    $row['name']['#markup'] = $entity->get('name')->value;
    $row['machine_name']['#markup'] = $machine_name;

    $route_name = 'entity.modules_info.status_toggle';
    $route_parameters = [
      'module_info' => $entity->id(),
    ];

    $row['status'] = [
      '#type' => 'link',
      '#title' => $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled'),
      '#url' => Url::fromRoute($route_name, $route_parameters),
      '#attributes' => [
        'id' => 'module-' . $machine_name . '-status',
        'class' => ['use-ajax'],
      ],
    ];
    $row['version']['#markup'] = $entity->get('version')->value;
    $row['sites']['#markup'] = $entity->get('sites')->value;
    $row['issues']['#markup'] = $entity->get('issues')->value;
    $row['bugs']['#markup'] = $entity->get('bugs')->value;
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'modules_info_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    parent::messenger()->addStatus($this->t('The order have been updated.'));
  }

}
