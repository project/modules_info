<?php

namespace Drupal\modules_info\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AnnounceCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\modules_info\Entity\ModulesInfo;

/**
 * Implements functions for Module Info module.
 */
class ModulesInfoController {
  use StringTranslationTrait;

  /**
   * Toggle status via AJAX.
   */
  public function statusToggle($module_info) {
    $module = ModulesInfo::load($module_info);
    // Toggle status.
    $module->set('status', !$module->get('status')->value)->save();

    // Get selector.
    $selector = '#module-' . $module->get('machine_name')->value . '-status';
    $response = new AjaxResponse();
    $locked = $module->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $response->addCommand(new HtmlCommand($selector, $locked));

    // Announce status.
    $t_args = ['@name' => $module->get('name')->value];
    $text = $module->get('status')->value ? $this->t('@name enabled.', $t_args) : $this->t('@name disabled.', $t_args);
    $response->addCommand(new AnnounceCommand($text));
    return $response;
  }

}
