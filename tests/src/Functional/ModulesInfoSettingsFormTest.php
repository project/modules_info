<?php

namespace Drupal\Tests\modules_info\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Url;

/**
 * Test Module Info Settings Form.
 *
 * @group modules_info
 */
class ModulesInfoSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'modules_info'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set up the test here.
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Test Module Info Settings Form Access.
   */
  public function testModulesInfoSettingsFormAccess() {
    // Test Module Info Settings Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer modules_info']);
    $this->drupalLogin($test_user);
    $this->drupalGet(URL::fromRoute('modules_info.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->addressEquals('admin/config/system/modules-info-settings');
    $this->assertSession()->pageTextContains('Modules info settings');
    $this->assertSession()->pageTextContains('Columns in Modules Info block');
    $this->assertSession()->pageTextContains('Id');
    $this->assertSession()->pageTextContains('Name');
    $this->assertSession()->pageTextContains('Machine name');
    $this->assertSession()->pageTextContains('Version');
    $this->assertSession()->pageTextContains('Sites');
    $this->assertSession()->pageTextContains('Issues');
    $this->assertSession()->pageTextContains('Bugs');
    $this->assertSession()->fieldExists('internal_css');
    $this->assertSession()->buttonExists('Save configuration');
    $this->drupalLogout();

    // Test Module Info Settings Form Access with Admin User
    // without required permission to access Module Info Settings Form.
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet(URL::fromRoute('modules_info.settings'));
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()
      ->pageTextContains('You are not authorized to access this page.');
    $this->drupalLogout();

    // Test OMDB API Module Settings Form Access with Anon User.
    $anon_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($anon_user);
    $this->drupalGet(URL::fromRoute('modules_info.settings'));
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()
      ->pageTextContains('You are not authorized to access this page.');
  }

  /**
   * Test Modules Info Settings Form Submit.
   */
  public function testModulesInfoSettingsFormSubmit() {
    // Test Module Info Settings Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer modules_info']);
    $this->drupalLogin($test_user);
    $this->drupalGet(URL::fromRoute('modules_info.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Modules info settings');
    $this->assertSession()->pageTextContains('Columns in Modules Info block');
    $this->assertSession()->pageTextContains('Id');
    $this->assertSession()->pageTextContains('Name');
    $this->assertSession()->pageTextContains('Machine name');
    $this->assertSession()->pageTextContains('Version');
    $this->assertSession()->pageTextContains('Sites');
    $this->assertSession()->pageTextContains('Issues');
    $this->assertSession()->pageTextContains('Bugs');
    $this->assertSession()->fieldExists('internal_css');
    $this->assertSession()->buttonExists('Save configuration');
  }

}
