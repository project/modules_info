<?php

namespace Drupal\modules_info\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Configure Modules Info settings for this site.
 */
class ModulesInfoSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'modules_info_module_info_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['modules_info.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $columns = $this->config('modules_info.settings')->get('columns');
    $form['columns'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Columns in Modules Info block'),
      '#options' => [
        'id' => $this->t('Id'),
        'name' => $this->t('Name'),
        'machine_name' => $this->t('Machine name'),
        'version' => $this->t('Version'),
        'sites' => $this->t('Sites'),
        'issues' => $this->t('Issues'),
        'bugs' => $this->t('Bugs'),
      ],
      '#default_value' => $columns ? $columns : [],
      '#description' => $this->t('Filters columns in the info block. No selected checkboxes means that the filter is not applied.'),
    ];

    $form['internal_css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use internal CSS styles for block'),
      '#default_value' => $this->config('modules_info.settings')->get('internal_css'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('modules_info.settings')
      ->set('columns', $form_state->getValue('columns'))
      ->save();

    $this->config('modules_info.settings')
      ->set('internal_css', $form_state->getValue('internal_css'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
