<?php

namespace Drupal\modules_info\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\modules_info\ModulesInfoService;

/**
 * Validates the ValidateMachineName constraint.
 */
class ValidateMachineNameConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface{
  /**
   * The Module Info Service.
   *
   * @var \Drupal\modules_info\ModulesInfoService
   */
  protected $moduleInfoService;

  /**
   * Creates a new ValidateMachineNameConstraintValidator instance.
   * 
   * @param \Drupal\modules_info\ModulesInfoService $module_info_service
   * The Module Info Service.
   */
  public function __construct(ModulesInfoService $module_info_service) {
    $this->moduleInfoService = $module_info_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('modules_info.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $entity = $items->getEntity();
    if (!$this->moduleInfoService->validateMachineName($entity->machine_name->value)) {
      $this->context->addViolation($constraint->validMachineName, ['%value' => $entity->machine_name->value]);
    }
  }
}
