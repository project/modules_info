<?php

namespace Drupal\modules_info\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if the module machine name exists or not.
 *
 * @Constraint(
 *   id = "ValidateMachineName",
 *   label = @Translation("Validate Machine Name", context = "Validation"),
 *   type = "string"
 * )
 */
class ValidateMachineNameConstraint extends Constraint {

  // The message that will be shown if the machine name does not exist.
  public $validMachineName = 'Machine name %value does not exist.';

}
