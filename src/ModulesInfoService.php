<?php

namespace Drupal\modules_info;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\modules_info\Entity\ModulesInfo;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\TransferException;

/**
 * Class ModulesInfoService implements common tasks with modules info entities.
 */
class ModulesInfoService {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Regex field patterns used for parsing values from Drupal module page.
   */
  const FIELD_PATTERNS = [
    'version' => '/View release notes\">(.*)?<\/a>/',
    'sites' => '/project\/usage\/{module}.*?\<strong\>(.*)?<\/strong> sites/',
    'issues' => '/project\/issues\/{module}\?categories\=All\">(.*)?<\/a>/',
    'bugs' => '/project\/issues\/{module}\?categories\=1\">(.*)?<\/a>/',
  ];

  /**
   * HTTP headers.
   */
  const HEADERS = [
    'headers' => [
      'Accept' => 'text/html',
      'Cache-control' => 'no-cache, must-revalidate',
    ],
  ];

  /**
   * Constructs a new ModulesInfoService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ClientInterface $http_client) {
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
  }

  /**
   * Cron callback which update modules info entities data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cron() {
    $modules = ModulesInfo::loadMultiple();
    /** @var \Drupal\modules_info\Entity\ModulesInfo $module */
    foreach ($modules as $module) {
      $module->save();
    }
  }

  /**
   * Updates single modules info entity.
   */
  public function update(EntityInterface $module) {
    $module_name = $module->get('machine_name')->value;
    $this->updateWeight($module);

    if ($html = $this->getModulePageHtml($module_name)) {
      $version = $this->parseModulePageValue('version', $html, $module_name);
      $sites = $this->parseModulePageValue('sites', $html, $module_name);
      $issues = $this->parseModulePageValue('issues', $html, $module_name);
      $bugs = $this->parseModulePageValue('bugs', $html, $module_name);

      $module->set('version', $version ?? '');
      $module->set('sites', $sites ?? 0);
      $module->set('issues', $issues ?? 0);
      $module->set('bugs', $bugs ?? 0);
    }
  }

  /**
   * Update weight.
   */
  private function updateWeight($module) {
    $modules = ModulesInfo::loadMultiple();
    $weight = $module->get('weight')->value;
    $max = -10000;
    $found = FALSE;
    foreach ($modules as $m) {
      $w = $m->get('weight')->value;
      $max = max($max, $w);
      if ($weight == $w && $module->id() != $m->id()) {
        $found = TRUE;
      }
    }
    if ($found) {
      $module->set('weight', $max + 1);
    }
  }

  /**
   * Gets module page body.
   *
   * @param string $module_name
   *   Module name.
   *
   * @return false|string|void
   *   Html code of the page.
   */
  private function getModulePageHtml(string $module_name) {
    $uri = 'https://www.drupal.org/project/' . $module_name;
    try {
      $response = $this->httpClient->get($uri, self::HEADERS);
      $data = (string) $response->getBody();
      if (!empty($data)) {
        return $data;
      }
    }
    catch (TransferException $e) {
      return FALSE;
    }
  }

  /**
   * Gets Drupal module usage page body.
   *
   * @return false|string|void
   *   Html code of the page.
   */
  private function getDrupalUsagePageHtml() {
    $uri = 'https://www.drupal.org/project/usage';
    try {
      $response = $this->httpClient->get($uri, self::HEADERS);
      $data = (string) $response->getBody();
      if (!empty($data)) {
        return $data;
      }
    }
    catch (TransferException $e) {
      return FALSE;
    }
  }

  /**
   * Parses given value from page body.
   *
   * @param string $type
   *   Value type.
   * @param string $html
   *   Page html code.
   * @param string $module_name
   *   Module name.
   *
   * @return array|false|mixed|string|string[]|null
   *   Return parsed value.
   */
  private function parseModulePageValue(string $type, string $html, string $module_name) {
    $matches = [];
    if (!isset(self::FIELD_PATTERNS[$type])) {
      return FALSE;
    }

    $pattern = str_replace('{module}', $module_name, self::FIELD_PATTERNS[$type]);
    if ($module_name == 'drupal' && $type == 'version') {
      $pattern = '/project\/drupal\/releases\/(.*)?\">/';
    }
    if ($module_name == 'drupal' && $type == 'sites') {
      if ($html = $this->getDrupalUsagePageHtml()) {
        $pattern = '/<td class=\"project-usage-numbers active\">(.*)?<\/td>/';
      }
    }
    if (preg_match($pattern, $html, $matches)) {
      if (!empty($matches[1])) {
        $value = $matches[1];
        // Filters the found value.
        switch ($type) {
          case 'sites':
            $value = str_replace(',', '', $value);
            break;

          case 'issues':
          case 'bugs':
            $value = preg_replace('/\D/', '', $value);
            break;
        }
        return $value;
      }
    }
    return FALSE;
  }

  /**
   * Validates the machine name of module.
   *
   * @param string $machine_name
   *   Machine name.
   *
   * @return true|false
   *   Returns bolean value
   */
  public function validateMachineName(string $machine_name) {
    $uri = 'https://www.drupal.org/project/' . $machine_name;
    try {
      $response = $this->httpClient->get($uri, self::HEADERS);
      return TRUE;
    }
    catch (TransferException $e) {
      if ($e->getCode() == 404) {
        return FALSE;
      }
    }
  }
}
