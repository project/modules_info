<?php

namespace Drupal\Tests\modules_info\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test Module Info Module Installation.
 *
 * @group modules_info
 */
class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'modules_info'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests Homepage after enabling Modules Info Module.
   */
  public function testHomepage() {
    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    $this->assertSession()->pageTextContains('Log in');
  }

  /**
   * Tests the Modules Info module uninstall.
   */
  public function testModuleUninstall() {
    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/modules/uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Modules Info');
    $this->submitForm(['uninstall[modules_info]' => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()
      ->pageTextContains('The selected modules have been uninstalled.');
    $this->assertSession()->pageTextNotContains('Modules Info');

    // Visit the frontpage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests Modules Info module reinstalling after being uninstalled.
   */
  public function testReinstallAfterUninstall() {
    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Uninstall the Modules Info module.
    $this->container->get('module_installer')
      ->uninstall(['modules_info'], FALSE);

    $this->drupalGet('/admin/modules');
    $page->checkField('modules[modules_info][enable]');
    $page->pressButton('Install');
    $assert_session->pageTextNotContains('Unable to install Modules Info');
    $assert_session->pageTextContains('Module Modules Info has been enabled');

    // Visit the frontpage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

}
